<?php

namespace CI\MyStalkerBundle\Resources\Classes;

class Stalker
{
	private $user_id;
	private $client_id;
	private $limit;
	private $search;
	private $secret_token;
	private $url_test;
	private $stock_profil;
	private $stock_position;
	private $stock_id;
	private $search_friend;
	private $search_url;

	public function __construct($id ,$search = null)
	{
		$this->search = $search;
		$this->user_id = "488816971";
		$this->client_id = "3100ab832d1c4a0e8959f7033b403e91";
		$this->secret_token = "488816971.3100ab8.faab88fbcdee43e581c99e9aaf019dc4";
		if ($id == 1)
			$this->search_url = $this->remplace_space("https://api.instagram.com/v1/users/search?q=".$this->search."&client_id=".$this->client_id."&count=50");
		else
		{
			$this->search_friend = "https://api.instagram.com/v1/users/".$this->user_id."/follows?access_token=".$this->secret_token."&max_timestamp=100";
		}
	}

	public function launch_stalker($id)
	{
		if ($id == 1)
			$this->anonymous_result_of_search();
		else 
			$this->location_of_your_friends();
	}

	private function location_of_your_friends()
	{
		$i = 0;
		if (($data = $this->init_curl($this->search_friend)) == null)
			echo "<p style='color:red;'> Connection out, veuillez recharger la page avec f5 </p>";
		else
		{
			if ($data->meta->code == 200)
			{
				foreach ($data->data as $index) 
				{
					$this->stock_id[$i] = $index->id;
					$i++;	
				}
				$this->give_position($this->stock_id);
			}
			else
			{
				echo "<p style='color:red;'> Error - Bad client_id </p>";				
			}
		}			
	}	

	private function give_position($stock_id)
	{
		$i = 0;
		$j = 0;

		while ($i != count($stock_id))
		{
			$data = $this->init_curl("https://api.instagram.com/v1/locations/".$stock_id[$i]."/media/recent?access_token=".$this->secret_token);
			if ($data == null)
			{

			}
			else
			{
				if ($data->meta->code == 200)
				{
					if ($data->data != null)
					{
						$result_tab[$j] = $data->data;
						$j++;
					}				
				}
				else
				{

				}
			}
			$i++;
		}
		foreach ($result_tab as $result) 
		{
			var_dump($result[0]->location);
		}
	}

	private function anonymous_result_of_search() 
	{
		$i = 0;
		if (($data = $this->init_curl($this->search_url)) == null)
			echo "<p style='color:red;'> Connection out, veuillez recharger la page avec f5 </p>";
		else
		{
			if ($data->meta->code == 200)
			{
				foreach ($data->data as $index)
				{
					$this->stock_profil['username'][$i] = $index->username;
					$this->stock_profil['full_name'][$i] = $index->full_name;	
					$this->stock_profil['id'][$i] = $index->id;
					$this->stock_profil['picture'][$i] = $index->profile_picture;
					$i++;		
				}
				$this->stock_profil['index'] = $i;
			}
			else
			{
				echo "<p style='color:red;'> Error - Bad client_id </p>";
			}
		}
	}

	private function init_curl($url)
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		return(json_decode(curl_exec($curl)));
	}

	private function remplace_space($remplace)
	{
		$i = 0;

		while ($i != strlen($remplace))
		{
			if ($remplace[$i] == ' ')
			{
				$remplace[$i] = '0';
				$remplace = $this->deplace_tab($remplace, $i);
				$remplace[$i++] = '%';
				$remplace = $this->deplace_tab($remplace, $i);
				$remplace[$i++] = '2';
			}
			$i++;
		}
		return ($remplace);	
	}

	private function deplace_tab($_string, $pos)
	{
		$_copy = " ";
		$i = $pos;
		$j = 0;

		while ($i < strlen($_string))
			$_copy[$j++] = $_string[$i++];
		$i = $pos;
		while ($i < strlen($_string))
			$_string[$i++] = ' ';
		$i = $pos + 1; 
		$j = 0;
		while ($j < strlen($_copy))
			$_string[$i++] = $_copy[$j++];
		return ($_string);			
	}

	public function get_stock_profil()
	{
		return ($this->stock_profil);
	}

	public function setter_stock_profil($_tab)
	{
		$this->stock_profil = $_tab;
	}
}

