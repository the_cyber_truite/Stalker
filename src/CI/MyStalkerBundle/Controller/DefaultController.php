<?php

namespace CI\MyStalkerBundle\Controller;

use CI\MyStalkerBundle\Resources\Classes\Stalker;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
		return $this->render('CIMyStalkerBundle:Default:index.html.twig');
    }

    public function search_personAction(Request $request)
    {
    	$tab_search = null;
    	$slug = $request->get('slug', null);
    	$defaultData = array(); 
    	$form = $this->createFormBuilder($defaultData)
    	->add('search', 'text')
    	->getForm();

		$form->handleRequest($request);
		if ($request->getMethod() === 'POST')
            {
                if ($form->isValid()) 
                {
					$data = $form->getData();
					$stalker = new Stalker(1, $data['search']);
					$stalker->launch_stalker(1);
					$tab_search = $stalker->get_stock_profil();
				}
			}		
    	return $this->render('CIMyStalkerBundle:Default:search_person.html.twig', array(
        'form' => $form->createView(),
        'tab_search' => $tab_search
        	));
    }

    public function search_locationAction(Request $request)
    {
    	$tab_search = null;
    	$slug = $request->get('slug', null);
    	$defaultData = array(); 
    	$form = $this->createFormBuilder($defaultData)
    	->add('search', 'text')
    	->getForm();

		$form->handleRequest($request);
		if ($request->getMethod() === 'POST')
            {
                if ($form->isValid()) 
                {
                    $data = $form->getData();
                    $stalker = new Stalker(2);
                    $stalker->launch_stalker(2);
				}
			}		
    	return $this->render('CIMyStalkerBundle:Default:search_location.html.twig', array(
        'form' => $form->createView()
        	));
    }
}

